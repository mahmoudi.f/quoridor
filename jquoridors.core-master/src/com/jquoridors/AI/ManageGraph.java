/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jquoridors.AI;


import com.jquoridors.core.board.BoardItem;
import com.jquoridors.core.board.Cell;
import com.jquoridors.core.board.Direction;

import java.util.*;
import org.jgrapht.*;
import org.jgrapht.graph.*;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;


/**
 * A simple introduction to using JGraphT.
 *
 * @author Barak Naveh
 */


public final class ManageGraph
{
    static String[] targetCells_UP   = {"a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1", "i1"};
    static String[] targetCells_DOWN = {"a9", "b9", "c9", "d9", "e9", "f9", "g9", "h9", "i9"};
    static String[] targetCells_RIGHT = {"i1", "i2", "i3", "i4", "i5", "i6", "i7", "i8", "i9"};
    static String[] targetCells_LEFT = {"a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9"};
    
    Graph<String, DefaultEdge> boardGraph;// = new SimpleGraph<>(DefaultEdge.class);

    public ManageGraph(TreeSet<BoardItem> boardcells){
        this.boardGraph = createGraph(boardcells);
    }
    
    public ManageGraph(Graph boardGraph){
        this.boardGraph = boardGraph;
    }
    
    private static String[] getTargetCells(Direction dir) {
        if (dir == Direction.UP)
            return targetCells_UP;
        else if (dir == Direction.DOWN)
            return targetCells_DOWN;  
        else if (dir == Direction.RIGHT)
            return targetCells_RIGHT; 
        else if (dir == Direction.LEFT)
            return targetCells_LEFT;
        return null;
    }

    public static void showGraph(Graph<String, DefaultEdge> boardGraph)
    {

//        System.out.println("Graph State is like:");
//        System.out.println(boardGraph.toString());
//        System.out.println();
                
    }

    public static String findShortestPathAction(String src, Direction dir, Graph graph) {
    
        // Find shortest path to each target Cells' position.
        List<GraphPath> paths = new ArrayList<>();
        List<Integer> pathsLength = new ArrayList<>();
        String[] targetCells = getTargetCells(dir);
        
        for (String dst : targetCells) {
            GraphPath gp = DijkstraShortestPath.findPathBetween(graph, src, dst);
            if(gp != null){
                paths.add(gp);
                pathsLength.add(gp.getLength());
            }
        }
        
        // find the shortest path among all
        int minIndex = pathsLength.indexOf(Collections.min(pathsLength));
        List nextPath = paths.get(minIndex).getVertexList();
        String nextAct = nextPath.get(1).toString();
        System.out.println("AI's next action is: "+ nextAct);
        return nextAct;
        
    }
    
    private Graph createGraph(TreeSet<BoardItem> boardcells) {
        
        Graph<String, DefaultEdge> g = new SimpleGraph<>(DefaultEdge.class);
        List<BoardItem> list = new ArrayList<>(boardcells);
        
        //add vertices
        for (int i = 0; i < boardcells.size(); i++) {
            Cell c = (Cell) list.get(i);
            g.addVertex(c.getPosition().toString());
        }        
        
        //add edges
        for (int i = 0; i < boardcells.size(); i++) {
                for (Direction dir : Direction.values()){
                    Cell c = (Cell) list.get(i);
                    if (c.getNeighbour(dir) != null)
                        g.addEdge(c.getPosition().toString(), c.getNeighbour(dir).getPosition().toString());
                }                    
            }
        
        return g;
    }


    public static Boolean isFloodable(String src, Graph graph, Direction dir) {

        GraphPath gp;// = null;
        String[] targetCells = getTargetCells(dir);
        
        for (String dst : targetCells) {
            gp = DijkstraShortestPath.findPathBetween(graph, src, dst);               
            if (gp != null)
                return true;
        }
        return false;
    }
    
    public Graph getGraph(){
        return this.boardGraph;
    }
    
    @Override
    public ManageGraph clone() throws CloneNotSupportedException {
        return new ManageGraph(boardGraph);
    }
    
    
    
    
}



