/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jquoridors.AI;

/**
 *
 * @author fm
 */
import com.jquoridors.core.Game;
import com.jquoridors.core.Player;
import com.jquoridors.core.WorldModelImpl;
import com.jquoridors.core.action.Action;
import com.jquoridors.core.action.move.PawnMove;
import com.jquoridors.core.board.Direction;
import com.jquoridors.core.board.Position;
import java.util.List;
import java.util.Random;
import org.jgrapht.Graph;
import org.jgrapht.graph.AbstractBaseGraph;

public class AI implements Player {
    private String name;
    
    public AI(){
        super();
    }
	
    public String getRandomAction(Game g) throws CloneNotSupportedException {

        Random rand = new Random();
        int actType = rand.nextInt(2);
        
        // Move
        if(actType==0){
            List<Position> validActions= g.getActionValidator().validMoves(g.getWorldModel().positionOf(g.getWorldModel().getCurrentPawn()));
            int rand_index = rand.nextInt(validActions.size());
            Position newPos = validActions.get(rand_index);
            System.out.println("here is AI's move: "+ newPos.toString());
            return newPos.toString();
        }
        //Block
        else{
            int row = rand.nextInt(8) + 1;        
        
            final String alphabet = "abcdefghi";
            final int N = alphabet.length();
            char col = alphabet.charAt(rand.nextInt(N));

            final String orientation = "hv";
            final int O = orientation.length();
            char orient = orientation.charAt(rand.nextInt(O));

            System.out.println("here is AI's move: "+String.valueOf(col)+String.valueOf(row)+String.valueOf(orient));
            return String.valueOf(col)+String.valueOf(row)+String.valueOf(orient);
        }        
}
    
    
    public String getShortestPathAction(Game g){
        
        Graph clonedGraph =(Graph)((AbstractBaseGraph)g.getWorldModel().getBoardGraph()).clone();
        Position currentPawnPosition = g.getWorldModel().getCurrentPawn().getPosition();
        List<Position> validPositions= g.getActionValidator().validMoves(currentPawnPosition);
        
        //reset node's edges considering pawn's valid moves (because in jump &jump-wall rules valid moves changes.)
        clonedGraph.removeVertex(currentPawnPosition.toString());
        clonedGraph.addVertex(currentPawnPosition.toString());
        validPositions.stream().forEach((p) -> {
            clonedGraph.addEdge(currentPawnPosition.toString(), p.toString());
        });
            
        //find all shortest path to oposite side of the board:        
        return ManageGraph.findShortestPathAction(g.getWorldModel().getCurrentPawn().getPosition().toString(), g.getWorldModel().getCurrentPawn().getEnd(), clonedGraph);
            
    }
     

    @Override
    public Action act(Game g) {
        
        Position actPos = new Position(getShortestPathAction(g));  
        return new PawnMove(actPos, g.getWorldModel().positionOf(g.getWorldModel().getCurrentPawn()));
//        else if (temp.length()==3){
//            String orient = String.valueOf(temp.charAt(2)).toUpperCase();
//            return new FenceMove(actPos, FenceDirection.valueOf(orient));
        
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return "AI agent";
    }

    @Override
    public void run() {
    }
    
  
	
}
