/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jquoridors.core.players;

import com.jquoridor.GUI.BoardUI;
import com.jquoridors.core.Game;
import com.jquoridors.core.Player;
import com.jquoridors.core.WorldModelImpl;
import com.jquoridors.core.action.Action;

/**
 *
 * @author fm
 */


public class Human implements Player {
    
    private String name; 
    
    public Human() {
        super();
    }

    public Human(String name) {
        this.name = name;
    }
    
    
        @Override
    public Action act(Game g) {
        
        // with commandline
//        String temp = "";
//        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
//        System.out.print("Enter the move for player "
//                + (this.getName()) + ": ");
//        try {
//            temp = in.readLine();
//        } catch (IOException e) {
//        }
//        Position actPos = new Position(temp);  
//        if (temp.length()==2)
//            return new PawnMove(actPos, worldModel.positionOf(worldModel.getCurrentPawn()));
//        else if (temp.length()==3){
//            String orient = String.valueOf(temp.charAt(2)).toUpperCase();
//            try {
//                return new FenceMove(actPos, (orient));
//            } catch (IllegalArgumentException e) {
//                return null;
//            }
//        }
//        return null;
//        
        
        //with GUI
        Action act = BoardUI.getInputs();
        return act;
    }

    @Override
    public void run() {

    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }
    
}
