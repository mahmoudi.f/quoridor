package com.jquoridors.core;

import com.jquoridors.AI.AI;
import com.jquoridors.core.GameType.playerType;
import com.jquoridors.core.action.*;
import com.jquoridors.core.board.BoardItem;
import com.jquoridors.core.board.BoardItemType;
import com.jquoridors.core.board.Direction;
import com.jquoridors.core.board.Pawn;
import com.jquoridors.core.board.Position;
import com.jquoridors.core.players.Human;
import static java.lang.System.exit;
import java.util.ArrayList;
import java.util.List;

public class Game implements Comparable<Game>/*implements Runnable*/ {

    protected List<Player> players = new ArrayList<>();
    private WorldModelImpl worldModel;
    private ActionValidatorImpl actionValidator;
    private ActionExecutorImpl actionExecutor;
    private Player winner;
    private Pawn winnerPawn;
    private List<playerType> playerTypes;
    boolean hasEnded = false;

    
    public Game(List<playerType> playerTypes){
        this.players = addPlayers(playerTypes);
        this.worldModel = new WorldModelImpl(players.size());
        actionValidator = new ActionValidatorImpl(Game.this);
        actionExecutor = new ActionExecutorImpl(Game.this);
        
    }
    
    public final List<Player> addPlayers(List<playerType> playerTypes){
        playerTypes.stream().forEach((player) -> {
            if (player == GameType.playerType.Human){
                players.add(new Human("human"));
            }
            else if (player == GameType.playerType.AI)
                players.add(new AI());
        });
        return players;
    }
    
    
    
    
//    @Override
    public void run() {
        
        if (players.size() != 2 && players.size() != 4) { throw new IllegalArgumentException(
                "Only 2 or 4 players are supported."); }
        
//        players.forEach(Runnable::run);
        
        do {
//                 players.forEach(player ->
//                {
//                    try {
//                        //Player player = players.get(worldModel.getBoard().getCurrent());
//                        perform(player, player.act(this)/*new PawnMove(new Position('f',9), new Position ('e', 9))*/);
//                        if (hasEnded()) {
//                            winner = player;
//                            
//                        }
//                    } catch (ActionIsInvalidException
//                            | ActionExecutionException ex) {
//                        System.err.println(ex.getMessage());
//                    }
//                    
//                });
            
            
                players.stream().anyMatch(player ->
                {
                    try {
                        perform(player, player.act(this));
                        if (hasEnded()) {
                            winner = player;
                            return true;
                            
                        }
                        
                    } catch (ActionIsInvalidException
                            | ActionExecutionException ex) {
                        System.err.println(ex.getMessage());
                    }
                    return false;
                });

        } while(!hasEnded());
        
    }

    private void perform(Player player, Action action)
            throws ActionIsInvalidException,
                    ActionExecutionException {

        //validate action
        while (action == null || !actionValidator.isValid(player, action)) {
            System.out.println("Invalid move, try again!");
            action = player.act(this);
            //throw new ActionIsInvalidException(player, action);
        }
        //perform action on board
        actionExecutor.execute(player, action);
    }

    /**
     * Checks each players position 
     * and returns true if any player won, false if not.
     * @return 
     */
    public boolean hasEnded() {
//        boolean hasEnded = false;
        Pawn current = null;
        for (BoardItem cur : worldModel.getBoard().getItems().get(BoardItemType.PAWN)) {
            current = (Pawn)cur;
            Position position = worldModel.positionOf(current);
            if (current.getEnd() == Direction.UP) {
                hasEnded = hasEnded || position.getRow() == 1;
            } else if (current.getEnd() == Direction.DOWN) {
                hasEnded = hasEnded || position.getRow() == 9;
            } else if (current.getEnd() == Direction.LEFT) {
                hasEnded = hasEnded || position.getCol() == 'a';
            } else if (current.getEnd() == Direction.RIGHT) {
                hasEnded = hasEnded || position.getCol() == 'i';
            }
        }
        if(hasEnded == true){
            setWinnerPawn(current);
            //hasEnded = true;
        }
        return hasEnded;
    }
    
    public ActionValidatorImpl getActionValidator(){
        return actionValidator;
    }
    
    public ActionExecutorImpl getActionExecutor(){
        return actionExecutor;
    }
    
    
    public WorldModelImpl getWorldModel(){
        return worldModel;
    }
    
    public List<Player> getPlayers(){
        return players;
    }
    
    protected void setWinnerPawn(Pawn pawn) {
        winnerPawn = pawn;
    }
    
    public Pawn getWinnerPawn(){
        return winnerPawn;
    }
    
    public boolean get_hasEnded(){
        return hasEnded;
    }

    @Override
    public int compareTo(Game o) {
        return 1;
    }

    
}
