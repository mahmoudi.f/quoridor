/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jquoridors.core;

import com.jquoridors.AI.ManageGraph;
import com.jquoridors.core.board.Board;
import com.jquoridors.core.board.BoardItem;
import com.jquoridors.core.board.BoardItemType;
import com.jquoridors.core.board.Cell;
import com.jquoridors.core.board.Pawn;
import com.jquoridors.core.board.Position;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;

/**
 *
 * @author fm
 */
public final class WorldModelImpl implements WorldModel{

    private Integer currentPawn = 0;
    private int[] numOfPawnsWall;
    Board board;
    Graph<String, DefaultEdge> graph;
    private Integer PLAYERSCOUNT;
    private final Integer WALLSCOUNT = 20;
    private final Integer DIMENSION = 9;
    private Integer allUsedWalls = 0;


    WorldModelImpl(int size) {
        board = new Board(size);
        PLAYERSCOUNT = size;
        init_walls();
        initGraph();
    }
    
    public void initGraph(){
        
        TreeSet<BoardItem> cells = board.getItems().get(BoardItemType.CELL);
        this.graph = new ManageGraph(cells).getGraph();
    }
    
    @Override
    public Board getBoard() {
        return board;
    }
    
    public final void init_walls(){
        //numOfPawnsWall
        this.numOfPawnsWall = new int[PLAYERSCOUNT];
        for(int i=0; i<numOfPawnsWall.length; i++)
            this.numOfPawnsWall[i] = WALLSCOUNT/PLAYERSCOUNT;
    }
    
    public void changeTurn(){
        currentPawn = (currentPawn + 1) % PLAYERSCOUNT;
    }
    
    
    public Integer getCurrent()
    {
        return currentPawn;
    }
    
    public Position positionOf(Pawn p) {
        
        TreeSet<BoardItem> cells = board.getItems().get(BoardItemType.CELL);
        Iterator iterator = cells.iterator();
        while (iterator.hasNext()){
            Cell c = (Cell) iterator.next();
            if (p.equals(c.getContainedPawn())) {
                    return new Position(c.cellNum);
            }
        }
        return null;
    }
     
     public Pawn getCurrentPawn(){

        TreeSet<BoardItem> pawns = board.getItems().get(BoardItemType.PAWN);
        List<BoardItem> list = new ArrayList<>(pawns);
        Pawn pawn = (Pawn) list.get(getCurrent());
        return pawn;
    }

    public Integer getNumOfPawnsWall(int i) {
        return numOfPawnsWall[i];
    }
    
     /**
     * Get the number of walls a given player has remaining.
     * @param pawnNum
     * @return 
     */
    public int getRemainingWalls(int pawnNum) {
        return numOfPawnsWall[pawnNum-1];       //pawnNum-1
    }
    
    public void decRemainingWalls(int pawnNum) {
         this.numOfPawnsWall[pawnNum]--;
    }

    public Graph<String, DefaultEdge> getBoardGraph() {
        return this.graph;
    }
    
    public void setBoardGraph(Graph<String, DefaultEdge> gg) {
        this.graph = gg;
    }

    public Integer getAllUsedWall() {
        return allUsedWalls;
    }

    public void increaseUsedWall() {
        if (allUsedWalls <20)
            allUsedWalls++;
    }
    
}
