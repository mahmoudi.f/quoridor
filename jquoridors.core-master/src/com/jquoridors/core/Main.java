package com.jquoridors.core;

import com.jquoridors.AI.AI;
import com.jquoridors.core.players.Human;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class Main {
	private static Game game;
	
        /*gets players and starts the game*/
	public static void main (String[] argv) throws IOException{
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Welcome to Quoridor Game!");
		String userInput = "";
		Integer playersCount =0;
		List <Player> players = new ArrayList <>();
		while(playersCount != 2 && playersCount != 4){
			userInput = "";
			while(!ifIsInteger(userInput)){
				System.out.print("Number of players (2/4): ");
				userInput = in.readLine();
			}

			playersCount = Integer.parseInt(userInput);
		}
		
		for (Integer i = 1; i<= playersCount; i++){
			userInput = "";
			while (!userInput.toLowerCase().matches("(ai?|h(uman)?)")) {
				System.out.print("Player " + i.toString() + " is a human or AI? ");
				userInput = in.readLine();
			}
			if (userInput.toLowerCase().matches("h(uman)?")){
				players.add(new Human());

                        System.out.print("Player " + i.toString() + " name: ");
                        userInput = in.readLine();
                        players.get(players.size()-1).setName(userInput);
			}
                        
                        else {
				
				players.add(new AI());
				//players[i - 1].setName("nana");
			}
		}
		//game = new Game(players);
		//game.run();
                
//		game.getWorldModel().getBoard().printBoard();
//                System.out.println("Player "+ game.getWinner().getColor() + " won the game!");
                
	}
	/**
	 * @param string which is meant to be an int
	 * @return false or true if correct parse
	 */
	static boolean ifIsInteger(String value)  
	{  
	     try  
	     {  
	         Integer.parseInt(value);  
	         return true;  
	      } catch(NumberFormatException nfe)  
	      {  
	          return false;  
	      }  
	}
}

