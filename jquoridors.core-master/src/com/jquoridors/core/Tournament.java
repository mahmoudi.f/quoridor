/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jquoridors.core;

import com.jquoridors.core.GameType.gameType1;
import com.jquoridors.core.GameType.gameType2;
import com.jquoridors.core.GameType.playerType;
import com.jquoridors.core.board.BoardItem;
import com.jquoridors.core.board.BoardItemType;
import com.jquoridors.core.board.Pawn;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;
import java.util.function.BinaryOperator;

/**
 *
 * @author fm
 */
public class Tournament extends Game{
    
   private int COUNT;
   TreeSet <Game> games = new TreeSet<>();
   Game currentGame;
   private ArrayList<Integer> winnersNumber;
//   public Boolean hasEnded = false;
   
   gameType1 ts;
   gameType2 cf;
   List<playerType> playerTypes;

    public Tournament(gameType1 ts, gameType2 cf, List<playerType> playerTypes) {
        super(playerTypes);

        this.ts = ts;
        this.cf = cf;
        this.playerTypes = playerTypes;
        
        if(ts == gameType1.Single)
            COUNT = 1;
        else if (ts == gameType1.Tournament)
        {
            if (playerTypes.size() == 2)
                COUNT =3;
            else if (playerTypes.size() == 4)
                COUNT = 5;
        }
        
        winnersNumber = new ArrayList(playerTypes.size());
        for(int i=0; i<COUNT; i++){
            addGames(playerTypes);
        }
    }
        
    public final void  addGames(List<playerType> playerTypes) {       
       games.add(new Game(playerTypes)); 
    }

   @Override
    public void run(){
        
        games.stream().map((game) -> {
//            System.out.println(">>>>> New Round Started <<<<<");
           return game;
       }).map((game) -> {
           seturrentGame(game);
           game.run();
           return game;
       }).map((game) -> {
           System.out.println("Player "+ game.getWinnerPawn().getColor().toString() + " WON the game!");
           return game;
       }).forEach((game) -> {
           winnersNumber.add(game.getWinnerPawn().getPawnNum());
       });
        
        // if all games ran successfully, set flag true:
        hasEnded = true;
        
        Integer maxOccurredElement = winnersNumber.stream()
        .reduce(BinaryOperator.maxBy((o1, o2) -> Collections.frequency(winnersNumber, o1) -
                        Collections.frequency(winnersNumber, o2))).orElse(null);
        System.out.println(maxOccurredElement);
        System.out.println(">>>>> Player "+ maxOccurredElement + " Won the tournament. <<<<<");
        
        //set winner of the tournament
        TreeSet<BoardItem> pawns = getWorldModel().getBoard().getItems().get(BoardItemType.PAWN);
        List<BoardItem> list = new ArrayList<>(pawns);
        
        Pawn winner = (Pawn) list.get(maxOccurredElement-1);
        setWinnerPawn(winner);
        
        
    }
    
     public void seturrentGame(Game game){
        currentGame = game;
    }
     
    public Game getCurrentGame(){
        return currentGame;
    }

    
   
}
