package com.jquoridors.core.action;

import com.jquoridors.core.Player;
import com.jquoridors.core.board.Board;

public interface ActionExecutor {

    void execute(Player player, Action action)
            throws ActionExecutionException;

    Board getBoard();
}
