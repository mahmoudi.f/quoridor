package com.jquoridors.core.action.move;


import com.jquoridors.core.board.Position;
import com.jquoridors.core.action.Action;

public abstract class AbstractMove implements Action {

    protected Position position;
//    protected String actionStr;

    public AbstractMove(Position position) {
//        if (position == null) {
//            throw new Exception();
//        }
        this.position = position;
    }
        
//    public AbstractMove (String actionStr){
//        this.actionStr = actionStr;
//    }
    
//    public AbstractMove(String actionStr, Position position){
//        this.actionStr = actionStr;
//        this.position = position;
//    }
    
    @Override
     public Position getPosition() {
        return position;
    }
        
}
