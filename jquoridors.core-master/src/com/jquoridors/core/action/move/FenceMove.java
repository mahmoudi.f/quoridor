package com.jquoridors.core.action.move;

import com.jquoridors.core.board.FenceDirection;
import com.jquoridors.core.board.Position;

public class FenceMove extends AbstractMove {

    private FenceDirection direction;

    public FenceMove(Position position, FenceDirection direction) {
        super(position);
        this.direction = direction;
    }
    
    
    // پوزیشنم میشه مثه این تو کانستراکتور کلاسش چک کرد درست باشه!
     public FenceMove(Position position, String direction) throws IllegalArgumentException{
        super(position);
        this.position = position;
        
        if (!isValidDirection(direction)) {
            throw new IllegalArgumentException("Invalid wall direction!");}
        else
            this.direction = direction.equalsIgnoreCase("H") ? FenceDirection.H : FenceDirection.V ;
        }
        

    public FenceDirection getFenceDirection() {
        return direction;
    }

    private boolean isValidDirection(String direction) {
        return direction.equalsIgnoreCase("V") ||  direction.equalsIgnoreCase("H");
    }
}
