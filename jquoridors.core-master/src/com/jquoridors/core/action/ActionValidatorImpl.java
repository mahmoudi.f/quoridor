/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jquoridors.core.action;

import com.jquoridors.AI.ManageGraph;
import com.jquoridors.core.Game;
import com.jquoridors.core.Player;
import com.jquoridors.core.action.move.FenceMove;
import com.jquoridors.core.action.move.PawnMove;
import com.jquoridors.core.board.Board;
import com.jquoridors.core.board.BoardItem;
import com.jquoridors.core.board.BoardItemType;
import com.jquoridors.core.board.Cell;
import com.jquoridors.core.board.Direction;
import com.jquoridors.core.board.FenceDirection;
import com.jquoridors.core.board.Pawn;
import com.jquoridors.core.board.Position;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jgrapht.Graph;
import org.jgrapht.graph.AbstractBaseGraph;
/**
 *
 * @author fm
 */
public class ActionValidatorImpl implements ActionValidator<Game> {

    Game g;
    @Override
    public Game getGame() {
        return g;
    }
    
    public ActionValidatorImpl(Game g){
        this.g = g;
    }

    @Override
    public Boolean isValid(Player player, Action action) {
        
        if (isValidActionString(action.getPosition().toString())) 
        {
            if(action instanceof PawnMove)
                return isValidMoveAction((PawnMove)action);
            else if(action instanceof FenceMove)    
                return isValidBlockAction((FenceMove)action);
            
        }
        return false;
    }
    
      public boolean isValidActionString(String actionStr) {
        boolean validity = (actionStr.length() >= 2);
        validity = validity && actionStr.charAt(0) >= 'a' && actionStr.charAt(0) <= 'i';
        validity = validity && actionStr.charAt(1) >= '1' && actionStr.charAt(1) <= '9';
        return validity;
    }
    
      
      public boolean isValidMoveAction(PawnMove pm)
    {
        boolean validity = true;
        Board board = getGame().getWorldModel().getBoard();
        
        TreeSet<BoardItem> pawns = board.getItems().get(BoardItemType.PAWN);
        List<BoardItem> list = new ArrayList<>(pawns);
        
        Pawn cur = (Pawn) list.get(g.getWorldModel().getCurrent());
        
        List<Position> adjacent = validMoves(g.getWorldModel().positionOf(cur));
        validity = validity && adjacent.contains(pm.getPosition());
        System.out.println("valid moves:    "+adjacent);
        
        return validity;
    }
      
      
    public boolean isValidBlockAction(FenceMove fm)  {
        
        boolean validity = true;
        Board board = getGame().getWorldModel().getBoard();
        Map<BoardItemType, TreeSet<BoardItem>> items = board.getItems();
        
        TreeSet<BoardItem> cells = board.getItems().get(BoardItemType.CELL);
        List<BoardItem> list = new ArrayList<>(cells);

        //walls are not allowed in "right" & "down" sides of the board:
        validity = validity && fm.getPosition().getCol() <= 'h';
        validity = validity && fm.getPosition().getRow() <= 8;
        
        //not pass walls count
        validity = validity && g.getWorldModel().getNumOfPawnsWall(g.getWorldModel().getCurrent()) > 0;
        
        //check if related cell does not have a wall
        Integer cn = (fm.getPosition().getRow()-1)*9 + (fm.getPosition().getCol() - 'a' + 1);            
        Cell northwest = (Cell) list.get(cn-1);
        validity = validity && (northwest.getWall() == null);
        
        if (validity) {
            
            cn = (fm.getPosition().getRow()-1)*9 + (fm.getPosition().getCol() - 'a' + 2);
            Cell northeast = (Cell) list.get(cn-1);

            cn = (fm.getPosition().getRow())*9 + (fm.getPosition().getCol() - 'a' + 1);
            Cell southwest = (Cell) list.get(cn-1);

            cn = (fm.getPosition().getRow())*9 + (fm.getPosition().getCol() - 'a' + 2);
            Cell southeast = (Cell) list.get(cn-1);

            if (fm.getFenceDirection() == FenceDirection.V) {
                validity = validity
                        && northwest.getNeighbour(Direction.RIGHT) != null;
                validity = validity
                        && southwest.getNeighbour(Direction.RIGHT) != null;
                
            } else if (fm.getFenceDirection() == FenceDirection.H) {
                validity = validity
                        && northwest.getNeighbour(Direction.DOWN) != null;
                validity = validity
                        && northeast.getNeighbour(Direction.DOWN) != null;
            } else {
                validity = false;
            }
            if (validity) {
                // Test if we can flood the board
                Graph copy = (Graph)((AbstractBaseGraph)g.getWorldModel().getBoardGraph()).clone();
                Graph testGraph = ActionExecutorImpl.updateGraphAfterBlock(copy, northwest.getPosition(), northeast.getPosition(), southwest.getPosition(), southeast.getPosition(), fm.getFenceDirection());

                TreeSet<BoardItem> pawns = items.get(BoardItemType.PAWN);
                for(BoardItem pawn : pawns) {
//                        System.out.println("pawn position:  "+ ((Pawn)pawn).getPawnPosition().toString());
                        if (!ManageGraph.isFloodable(((Pawn) pawn).getPosition().toString(), testGraph, ((Pawn) pawn).getEnd())) {
                            System.out.println("board is not floodable!!!" + " try another action");
                            validity = false;
                        }
                    }
            }

        }
        return validity;
    }
    
    /**
     * Get a list of valid relocatePiece cells from a position
     * @param p
     * @return reachable empty squares
     */
        
    public List<Position> validMoves(Position p) {
        LinkedList<Position> adjacent = new LinkedList<>();
        LinkedList<Position> moves = new LinkedList<>();
        LinkedList<Direction> dirs = new LinkedList<>();
        
        TreeSet<BoardItem> cells = getGame().getWorldModel().getBoard().getItems().get(BoardItemType.CELL);
        List<BoardItem> list = new ArrayList<>(cells);
        Integer cn = Position.convertPositionToCellnum(p);
        
        Cell current = (Cell) list.get(cn-1);
        Cell neighbour;
        Direction d;
        for (Direction dir : Direction.values()) {
            neighbour = current.getNeighbour(dir);
            if (neighbour != null) {
                boolean isAdded = adjacent.add(neighbour.getPosition());
                dirs.add(dir);
            }
        }
        
        for (Position pos : adjacent) {
            d = dirs.removeFirst();
            if (playerAt(pos) != null) {
                moves.addAll(Arrays.asList(jump(pos, d, false)));
            } else
            {
                moves.add(pos);
            }
        }
        return moves;
    }
    
    /**
     * Implement the special rules of player adjacency in quoridor.
     * 
     * @param p starting position
     * @param d direction to jump
     * @return list of possible moves
     */
    private Position[] jump(Position p, Direction d, boolean giveUp) {
        if (!wallExists(p, d) && playerAt(p.adjacentPos(d)) == null) {
            Position[] rval = new Position[1];
            rval[0] = p.adjacentPos(d);
            return rval;
        } else if (giveUp) {
            Position[] rval = new Position[0];
            return rval;
        } else {
            LinkedList<Position> moves = new LinkedList<>();
            for (Direction dir : Direction.values()) {
                if (!dir.equals(d)) {
                    for (Position pos : jump(p, dir, true)) {
                        moves.add(pos);
                    }
                }
            }
            Position rval[] = new Position[moves.size()];
            return moves.toArray(rval);
        }
    }

        public boolean wallExists(Position p, Direction d) {
            
            TreeSet<BoardItem> cells = new TreeSet<>();
            Map<BoardItemType, TreeSet<BoardItem>> items = g.getWorldModel().getBoard().getItems();
            cells = items.get(BoardItemType.CELL);
            List<BoardItem> list = new ArrayList<>(cells);
//            Integer cn = (p.getRow()-1)*9 + (p.getCol() - 'a' + 1);    
            Integer cn = Position.convertPositionToCellnum(p);
            Cell c = (Cell) list.get(cn-1);
        
            return c.getNeighbour(d) == null;
    }
        
     /**
     * Get the id of the player at the specified position
     * @param p
     * @return 
     */
    public Integer playerAt(Position p) {
        
        TreeSet<BoardItem> cells = new TreeSet<>();
        Map<BoardItemType, TreeSet<BoardItem>> items = g.getWorldModel().getBoard().getItems();
        cells = items.get(BoardItemType.CELL);
        List<BoardItem> list = new ArrayList<>(cells);
//        Integer cn = (p.getRow()-1)*9 + (p.getCol() - 'a' + 1);
        Integer cn = Position.convertPositionToCellnum(p);
        Cell c = (Cell) list.get(cn-1);
        
        Pawn pl = c.getContainedPawn();
        if (pl != null) {
            return pl.getPawnNum();
        } else {
            return null;
        }
    }


      
}
