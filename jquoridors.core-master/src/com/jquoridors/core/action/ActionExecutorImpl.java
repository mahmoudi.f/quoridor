/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jquoridors.core.action;

import com.jquoridors.AI.ManageGraph;
import com.jquoridors.core.Game;
import com.jquoridors.core.Player;
import com.jquoridors.core.action.move.FenceMove;
import com.jquoridors.core.action.move.PawnMove;
import com.jquoridors.core.board.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jgrapht.Graph;
import org.jgrapht.graph.AbstractBaseGraph;
import org.jgrapht.graph.DefaultEdge;

/**
 *
 * @author fm
 */

public class ActionExecutorImpl implements ActionExecutor{

    Game g;
    Board board;

    public ActionExecutorImpl(Game g){
        this.g = g;
        this.board = this.g.getWorldModel().getBoard();
    }
    
    public Board getBoard(){
        return board;
    }

    @Override
    public void execute(Player player, Action action) throws ActionExecutionException {
        if(action instanceof PawnMove){
            move((PawnMove)action);
        }
        else if(action instanceof FenceMove){
            try {
                block((FenceMove)action);
                g.getWorldModel().decRemainingWalls(g.getWorldModel().getCurrent());
            } catch (CloneNotSupportedException ex) {
                Logger.getLogger(ActionExecutorImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }      
        
        
        g.getWorldModel().changeTurn();
        printBoard();
    }

    

    /**
     * Move the current piece to the specified position.
     */
    void move(PawnMove pm) {

        Pawn p = g.getWorldModel().getCurrentPawn();
        Position from = pm.getCurrentPosition();
        Position to = pm.getPosition();
        TreeSet<BoardItem> cells = new TreeSet<>();
        cells = board.getItems().get(BoardItemType.CELL);
        List<BoardItem> list = new ArrayList<>(cells);
        
//        Integer cn = (to.getRow()-1)*9 + (to.getCol() - 'a'+1);
        Integer cn = Position.convertPositionToCellnum(to);
        Cell c = (Cell)list.get(cn-1);
        c.setPawn(p);

        System.out.println("from pos:   "+ pm.getCurrentPosition().toString());
        System.out.println("to pos:   "+ pm.getPosition().toString());
//        cn = (from.getRow()-1)*9 + (from.getCol() - 'a'+1); 
        cn = Position.convertPositionToCellnum(from);
        c = (Cell)list.get(cn-1);       
        c.setPawn(null);
        p.setPosition(to);
    }
    
    
    
    /**
     * Place a wall at the given position.
     */       
    void block(FenceMove fm) throws CloneNotSupportedException {
        
        Position to = fm.getPosition();
        FenceDirection fenceDir = fm.getFenceDirection();
        
        TreeSet<BoardItem> cells = board.getItems().get(BoardItemType.CELL);
        TreeSet<BoardItem> walls = board.getItems().get(BoardItemType.WALL);
        List<BoardItem> wallsList = new ArrayList<>(walls);
        List<BoardItem> cellsList = new ArrayList<>(cells);
        
        Integer cn = (to.getRow()-1)*9 + (to.getCol() - 'a'+1);
        Cell northwest = (Cell)cellsList.get(cn-1);
        
        cn = (to.getRow()-1)*9 + (to.getCol()+1 - 'a'+1);
        Cell northeast = (Cell)cellsList.get(cn-1);
                
        cn = (to.getRow())*9 + (to.getCol() - 'a'+1);    
        Cell southwest = (Cell)cellsList.get(cn-1);
        
        cn = (to.getRow())*9 + (to.getCol()+1 - 'a'+1);
        Cell southeast = (Cell)cellsList.get(cn-1);
        
        
        if (fenceDir == FenceDirection.V) {
            northwest.setNeighbour(Direction.RIGHT, null);
            northeast.setNeighbour(Direction.LEFT, null);
            southwest.setNeighbour(Direction.RIGHT, null);
            southeast.setNeighbour(Direction.LEFT, null);
            
            
        } else if(fenceDir == FenceDirection.H){
            northwest.setNeighbour(Direction.DOWN, null);
            northeast.setNeighbour(Direction.DOWN, null);
            southwest.setNeighbour(Direction.UP, null);
            southeast.setNeighbour(Direction.UP, null);

        }

        //set related cell's wall
        northwest.setWall(new Wall(to, fenceDir));
        
        //set board's walls' position and direction:
        g.getWorldModel().increaseUsedWall();
        Wall wall = (Wall) wallsList.get(g.getWorldModel().getAllUsedWall()-1);
        wall.setPosition(to);
        wall.setWallDirection(fenceDir);

        //update graph
        g.getWorldModel().setBoardGraph(updateGraphAfterBlock(g.getWorldModel().getBoardGraph(), northwest.getPosition(), northeast.getPosition(), southwest.getPosition(), southeast.getPosition(), fenceDir));
    }
    
    
    public static Graph updateGraphAfterBlock(Graph<String, DefaultEdge> graph, Position nw, Position ne, Position sw, Position se, FenceDirection fdir){
        
        if(fdir == FenceDirection.V){
            graph.removeEdge(nw.toString(), ne.toString());
            graph.removeEdge(sw.toString(), se.toString());
        }
        else if(fdir == FenceDirection.H){
            graph.removeEdge(nw.toString(), sw.toString());
            graph.removeEdge(ne.toString(), se.toString());
        }
        ManageGraph.showGraph(graph);
        return graph;
    }

    
        public void printBoard() {
        
            Map<BoardItemType, TreeSet<BoardItem>> items = g.getWorldModel().getBoard().getItems();
            items.get(BoardItemType.PAWN).stream().forEach((current) -> {
                System.out.println("Player " + ((Pawn)current).getPawnNum() +" has "
                        + g.getWorldModel().getRemainingWalls(((Pawn)current).getPawnNum()) + " walls remaining.");
        });
        System.out.println(" [4m a b c d e f g h i [24m");
        for (int i = 0; i < 9; i++) {
            System.out.print(i + 1);
            System.out.print("|");
            
            List<BoardItem> list = new ArrayList<>(items.get(BoardItemType.CELL));
            for (int j = 0; j < 9; j++) {
                BoardItem cell = list.get(i*9+j);
                String cellname;
                if (((Cell)cell).getContainedPawn() == null) {
                    cellname = " ";
                } else {
                    cellname = String.valueOf(((Cell)cell).getContainedPawn().getPawnNum());
                    if (((Cell)cell).getContainedPawn().getPawnNum() == g.getWorldModel().getCurrent()) {
                        cellname = "[1m" + cellname + "[0m";
                    }
                }
                if (((Cell)cell).getNeighbour(Direction.DOWN) != null && i != 8) {
                    
                    cellname = "[24m" + cellname;
                } else {
                    
                    cellname = "[4m" + cellname;
                    if (((Cell)cell).getContainedPawn() != null
                            && ((Cell)cell).getContainedPawn().getPawnNum() == g.getWorldModel().getCurrent()) {
                        cellname += "[4m";
                    }
                }
                if (((Cell)cell).getNeighbour(Direction.RIGHT) == null) {
                    cellname += "|";
                } else if (i != 8) {
                    cellname += ".";
                } else {
                    cellname += " ";
                }
                System.out.print(cellname);
            }
            System.out.println("[24m");
        }
        //System.out.println("It is " + pieces[current].getColor() + "'s turn.");
    }

    
    
    
    
}
