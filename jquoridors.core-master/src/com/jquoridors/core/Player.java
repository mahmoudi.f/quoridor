package com.jquoridors.core;

import com.jquoridors.core.action.Action;

public interface Player extends Runnable {

    Action act(Game g);
    void setName(String name);
    String getName();
}
