package com.jquoridors.core.board;

public class Cell extends BoardItem {

    private Cell[] neighbours = new Cell[4];
    Pawn pawn;
    Wall wall;
    public Integer cellNum;
    
    public Cell() {
    }
    
    public Cell(Position p) {
        super(p);
    }

    Cell(Integer cellCounter) {
        this.position = new Position(cellCounter);
        this.cellNum = cellCounter;
    }
    
    public void setPawn(Pawn pawn) {
        this.pawn = pawn;
    }
    
    public void setWall(Wall wall){
        this.wall = wall;
    }

    public Wall getWall(){
        return wall;
    }
    
    public Pawn getContainedPawn() {
        return pawn;
    }

    public Cell getNeighbour(Direction direction) {
        return neighbours[direction.index()];
    }
    
    public void setNeighbour(Direction direction, Cell c) {
        neighbours[direction.index()] = c;
    }
        
}

