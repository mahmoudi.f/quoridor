package com.jquoridors.core.board;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import javafx.scene.paint.Color;

public class Board {

    private  Map<BoardItemType, TreeSet<BoardItem>> items = new HashMap<>();
    private Integer PLAYERSCOUNT;
    private final Integer WALLSCOUNT = 20;
    private final Integer DIMENSION = 9;
    Direction[] PAWNS_DEFAULT_DIR = {Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT};
    Position[] PAWNS_DEFAULT_POS = { new Position('e', 9), new Position('e', 1), new Position('i', 5), new Position('a', 5)};
    Color[] PAWNS_COLOR = {Color.BLUE, Color.RED, Color.GREEN, Color.YELLOW};

    public Board(Integer PLAYERSCOUNT){
        this.PLAYERSCOUNT = PLAYERSCOUNT;
        initBoard();
    }
    public final void initBoard() {
                
        //walls
        TreeSet<BoardItem> walls = new TreeSet<>();
        for (Integer wallCounter = 1; wallCounter <= WALLSCOUNT; ++wallCounter) {
            walls.add(new Wall(wallCounter));
        }
        items.put(BoardItemType.WALL, walls);
        
        //////////////////////////////////////////////////////////////
        //pawns
        TreeSet<BoardItem> pawns = new TreeSet<>();
        for (Integer pawnCounter = 1; pawnCounter <= PLAYERSCOUNT; ++pawnCounter) {
            pawns.add(new Pawn(PAWNS_DEFAULT_POS[pawnCounter-1], pawnCounter, PAWNS_COLOR[pawnCounter-1], PAWNS_DEFAULT_DIR[pawnCounter-1]));
        }
        items.put(BoardItemType.PAWN, pawns);
        
        //////////////////////////////////////////////////////////////
        
        
        //board
        TreeSet<BoardItem> cells = new TreeSet<>();
        for (Integer cellCounter = 1; cellCounter <= DIMENSION*DIMENSION; ++cellCounter) {
            cells.add(new Cell(cellCounter));
        }
            
        //set cells'neighbours
        cells = setCellsNeighbours(cells);
       
        //set cells' pawns
        List<BoardItem> list = new ArrayList<>(cells);
        Iterator iterator = pawns.iterator();
        while (iterator.hasNext()){
            Pawn p = (Pawn) iterator.next();
            Integer cn = Position.convertPositionToCellnum(p.position);
            //Integer cn = (p.position.getRow()-1)*9 + (p.position.getCol() - 'a'+1);
            Cell c = (Cell)list.get(cn-1);
            c.setPawn(p);
        }
        items.put(BoardItemType.CELL, cells);
        
    }

    
    public TreeSet<BoardItem> setCellsNeighbours(TreeSet<BoardItem> cells){
        
        List<BoardItem> list = new ArrayList<>(cells);
        Iterator iterator = cells.iterator();
        while (iterator.hasNext()){
            Cell c = (Cell)iterator.next();            
            if(c.cellNum%9 != 0)
                c.setNeighbour(Direction.RIGHT, (Cell)list.get(c.cellNum));

            if(c.cellNum%9 != 1)
                c.setNeighbour(Direction.LEFT, (Cell)list.get(c.cellNum-2));
            
            if(10 <= c.cellNum && c.cellNum <= 81)
                c.setNeighbour(Direction.UP, (Cell)list.get(c.cellNum-DIMENSION-1));
            
            if(1 <= c.cellNum && c.cellNum <= 72)
                c.setNeighbour(Direction.DOWN, (Cell)list.get(c.cellNum+DIMENSION-1));
            
        }
            return cells;
    }
    
//    private TreeSet<BoardItem> setCellsPawns(TreeSet<BoardItem> cells, TreeSet<BoardItem> pawns) {
//         
//        List<BoardItem> list = new ArrayList<>(cells);
//        Iterator iterator = pawns.iterator();
//        while (iterator.hasNext()){
//            Pawn p = (Pawn) iterator.next();
//            Integer cn = (p.position.getRow()-1)*9 + (p.position.getCol() - 'a'+1);
//            Cell c = (Cell)list.get(cn);
//            c.setPawn(p);
//        }
//        return (TreeSet<BoardItem>) list;
//    }
    
    private Board(Map<BoardItemType, TreeSet<BoardItem>> items) {
        this.items = items;
        
    }
    
    
    public  Map<BoardItemType, TreeSet<BoardItem>> getItems(){
        return items;
    }
    
//    public Position positionOf(Pawn p) {
//        
//        TreeSet<BoardItem> cells = items.get(BoardItemType.CELL);
//        Iterator iterator = cells.iterator();
//        while (iterator.hasNext()){
//            Cell c = (Cell) iterator.next();
//            if (p.equals(c.getContainedPawn())) {
//                    return new Position(c.cellNum);
//            }
//        }
//        return null;
//    }

//    public Integer[] getNumOfPawnsWall() {
//        return this.numOfPawnsWall;
//    }
    
//    public Pawn getCurrentPawn(){
//        
//        TreeSet<BoardItem> pawns = items.get(BoardItemType.PAWN);
//        List<BoardItem> list = new ArrayList<>(pawns);
//        Pawn pawn = (Pawn) list.get(getCurrent());
//        return pawn;
//    }
//    
//    public Pawn getCurrentPawn(Integer current){
//        TreeSet<BoardItem> pawns = items.get(BoardItemType.PAWN);
//        List<BoardItem> list = new ArrayList<>(pawns);
//        Pawn pawn = (Pawn) list.get(current);
//        return pawn;
//    }
//    
//     public Integer getCurrent(){
//        return currentPawn;
//    }
     
//    public void setCurrentPawn(Integer current){
//        this.currentPawn = current;
//    }
}
