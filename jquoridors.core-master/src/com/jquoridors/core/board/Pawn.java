package com.jquoridors.core.board;

import javafx.scene.paint.Color;


public class Pawn extends BoardItem {

    private Color color;
    private int pawnNum;
    private Direction dir;
    private BoardItem boardItem;
    
    public Pawn() {
    }
    
    public Pawn(Position p, int pieceNum, Color c, Direction dir) {

        super(p);
        this.dir = dir;
        this.color = c;
        this.pawnNum = pieceNum;
    }
    
    public Pawn(Position p) {
        super(p);
    }

    
    /**
     * Get the player's ID.
     * @return 
     */
    public final int getPawnNum() {
        return pawnNum;
    }

    public void setPawnPosition(Position pos){
        this.position = pos;
    }

    public Color getColor() {
        return color;
    }
    
    /**
     * Get the direction of the target side. when a player won the game
     * @return 
     */
    public final Direction getEnd() {
        return this.dir;
    }

}

