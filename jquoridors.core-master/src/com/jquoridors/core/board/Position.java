package com.jquoridors.core.board;

import java.util.Objects;

public class Position {

    Character col;
    Integer row;

    public Position(Character col, Integer row) {
        this.col = col;
        this.row = row;
    }

    public Character getCol() {
        return col;
    }

    public void setCol(Character col) {
        this.col = col;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }
    
    
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();        
        s.append(this.col);
        s.append(this.row);
        
        return s.toString();
    }
    
    /**
     * Create a new position from the string representation of a position.
     * @param move
     */
    public Position(String move) throws IllegalArgumentException {
        this.col = move.toCharArray()[0];
        this.row = Character.getNumericValue(move.charAt(1));
    }
    
    public Position(Integer cellNum){
        this.col = (char)Integer.parseInt(String.valueOf((cellNum-1)%9 + 'a'));        
        this.row = (cellNum-1)/9 + 1;
    }
    
    @Override
    public boolean equals(Object m) {
        if (!(m instanceof Position)) {
            return false;
        } else {
            Position move = (Position) m;
            boolean same = Objects.equals(move.getRow(), getRow()) && Objects.equals(move.getCol(), getCol());
            return same;
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.col);
        hash = 89 * hash + Objects.hashCode(this.row);
        return hash;
    }
            
     /**
     * Get the adjacent cell in a given direction.
     * @param d
     * @return 
     */
    public Position adjacentPos(Direction d) {
        Integer newrow = this.row;
        Character newcol = this.col;
        
        if (d == Direction.DOWN) newrow++;
        else if (d == Direction.UP) newrow--;
        else if (d == Direction.LEFT) newcol--;
        else if (d == Direction.RIGHT) newcol++;
        
        return new Position(newcol, newrow);
    }
    
    
    public static Integer convertPositionToCellnum(Position pos){
        
        Integer cn = (pos.getRow()-1)*9 + (pos.getCol() - 'a'+1);
        return cn;
    }
    
    
    
    
    
    
    
    
    
    
    
}
