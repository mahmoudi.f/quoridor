package com.jquoridors.core.board;

public class Wall extends BoardItem{

    FenceDirection fenceDirection = FenceDirection.NO_DIRECTION;
    Integer wallNum;
//    Cell[] neighbours;


    public Wall(Position p, FenceDirection fd) {
//        super(p);
        this.position = p;
        this.fenceDirection = fd;
    }
    
    public Wall(Integer wallNum) {
        super();
        this.wallNum = wallNum;
    }


    public void setWallDirection(FenceDirection dir){
        this.fenceDirection = dir;
    }

    public FenceDirection getOrientation(){
        return this.fenceDirection;
    }


}
