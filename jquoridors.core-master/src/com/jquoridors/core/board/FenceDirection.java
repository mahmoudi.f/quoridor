package com.jquoridors.core.board;

public enum FenceDirection {
	H(0), V(1), NO_DIRECTION(2);
    private final int index;   

    FenceDirection(int index) {
        this.index = index;
    }

    private int index() { 
        return index; 
    }
    }