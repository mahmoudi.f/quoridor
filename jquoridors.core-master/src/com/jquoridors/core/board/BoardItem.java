package com.jquoridors.core.board;

public abstract class BoardItem implements Comparable<BoardItem>{

    protected Position position;
    public BoardItem(Position p){
        this.position = p;
    }
    
    public BoardItem(){
        
    }
    
    public Position getPosition(){
        return this.position;
    }

    @Override
    public int compareTo(BoardItem o) {
        if (o instanceof Pawn){
            Pawn p = (Pawn)o;
            return p.getPawnNum();}
        else if(o instanceof Wall){
            Wall w = (Wall)o;
            return w.wallNum;
        }
        else if(o instanceof Cell){
            Cell c = (Cell)o;
            return c.getPosition().getRow() + c.getPosition().getCol();
        }
       
        return 0;
    }

    public void setPosition(Position position){
        this.position = position;
    }
}
