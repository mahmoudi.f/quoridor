package com.jquoridor.GUI;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author fm
 */
import com.jquoridors.core.board.Position;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;


/**
 * @author Almas Baimagambetov (almaslvl@gmail.com)
 */
public class PawnUI extends StackPane {

    private Color color;
    private Position position;
    
    private static final int CELL_SIZE = 60;
    private static final int WALL_WIDTH = 15;

    public Color getColor() {
        return color;
    }

    public PawnUI(Color color, Position pos) {
        
        this.color = color;                        
        setPosition(pos);

        Ellipse ellipse = new Ellipse(CELL_SIZE * 0.3125, CELL_SIZE * 0.26);
        ellipse.setFill(color == Color.RED
                ? Color.RED : Color.YELLOW);

        ellipse.setStroke(Color.BLACK);
        ellipse.setStrokeWidth(CELL_SIZE * 0.03);

        ellipse.setTranslateX((CELL_SIZE - CELL_SIZE * 0.3125 * 2) / 2);
        ellipse.setTranslateY((CELL_SIZE - CELL_SIZE * 0.26 * 2) / 2);

        getChildren().addAll(ellipse);
    }

    public final void setPosition(Position pos) {
        this.position = pos;
        Integer xx = pos.getRow();
        Integer yy = pos.getCol() - 'a' + 1;
        
        int y = (yy-1) * (CELL_SIZE + WALL_WIDTH);
        int x = (xx-1) * (CELL_SIZE + WALL_WIDTH);
        relocate(y, x);
    }
  
}
