package com.jquoridor.GUI;

import com.jquoridors.core.board.FenceDirection;
import com.jquoridors.core.board.Position;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class WallUI extends Rectangle {


    public static final int WALL_HEIGHT = 15;
    public static final int WALL_WIDTH = 60;
    private Position position;
    private int x,y;
    
    FenceDirection fenceDirection = FenceDirection.NO_DIRECTION;

    public WallUI(int x, int y, FenceDirection fenceDir) {

        this.x = x;
        this.y = y;

        this.position = new Position((Character)((char)(x + 'a')), y+1);
        
        if (fenceDir == FenceDirection.H) {
            relocate(x * (CellUI.CELL_SIZE+WALL_HEIGHT), y * (CellUI.CELL_SIZE+WALL_HEIGHT) + CellUI.CELL_SIZE);
            setHeight(WALL_HEIGHT);
            setWidth(WALL_WIDTH);
        } else if (fenceDir == FenceDirection.V){
            relocate(x * (CellUI.CELL_SIZE+WALL_HEIGHT)+CellUI.CELL_SIZE, y *(CellUI.CELL_SIZE+WALL_HEIGHT));
            setWidth(WALL_HEIGHT);
            setHeight(WALL_WIDTH);
        }
        
        setFill(Color.LIGHTGRAY);
        
    }

    public Position getPosition(){
       return this.position;
   }

}

