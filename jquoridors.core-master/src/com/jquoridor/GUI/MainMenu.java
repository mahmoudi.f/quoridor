/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jquoridor.GUI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.geometry.Insets;
import javafx.scene.Node;

/**
 *
 * @author fm
 */
import com.jquoridors.core.GameType.*;
public class MainMenu extends Application{
    
    
    private Scene scene;
    VBox vbox;
    
    
    playerType player;
    
    gameType1 TS;// = gameType1.Tournament;
    gameType2 CF;// = gameType2.Classic;
    int number;
    
    
public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage stage) {
      
      
    stage.setTitle("Quoridor");
      
    scene = new Scene(new Group());
    stage.setWidth(300);
    stage.setHeight(500);

    //////////////////////////////////////////////////////////////////////
    
    
    //Tournament & Single Game
    final ToggleGroup group1 = new ToggleGroup();
    
    RadioButton rb1 = new RadioButton("Tournament");
    rb1.setToggleGroup(group1);
    rb1.setUserData(gameType1.Tournament);

    RadioButton rb2 = new RadioButton("Single Game");
    rb2.setToggleGroup(group1);
    rb2.setUserData(gameType1.Single);
    
    rb2.setSelected(true);
    TS = gameType1.valueOf(group1.getSelectedToggle().getUserData().toString());
    
    group1.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
       @Override 
        public void changed(ObservableValue<? extends Toggle> ov,
          Toggle old_toggle, Toggle new_toggle) {
        if (group1.getSelectedToggle() != null) {
          System.out.println("in group1:    "+group1.getSelectedToggle().getUserData().toString());
          TS = gameType1.valueOf(group1.getSelectedToggle().getUserData().toString());
        }
      }
    });
    
    ///////////////////////////////////////////////////////////////////////////////
    //Classic & Flaggy
    final ToggleGroup group2 = new ToggleGroup();
    
    RadioButton rb3 = new RadioButton("Classic");
    rb3.setToggleGroup(group2);
    rb3.setUserData(gameType2.Classic);
    
    RadioButton rb4 = new RadioButton("Flaggy");
    rb4.setToggleGroup(group2);
    rb4.setUserData(gameType2.Flaggy);
    
    rb3.setSelected(true);
    CF = gameType2.valueOf(group2.getSelectedToggle().getUserData().toString());
    
    group2.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
       @Override 
        public void changed(ObservableValue<? extends Toggle> ov,
          Toggle old_toggle, Toggle new_toggle) {
        if (group2.getSelectedToggle() != null) {
          System.out.println("in group2:    "+group2.getSelectedToggle().getUserData().toString());
          CF = gameType2.valueOf(group2.getSelectedToggle().getUserData().toString());
        }
      }
    });
    
    ////////////////////////////////////////////////////////////////////////////
    
    //Number of players
    final ToggleGroup group3 = new ToggleGroup();
    
    RadioButton rb5 = new RadioButton("2 Players");
    rb5.setToggleGroup(group3);
    rb5.setUserData(2);
    
    RadioButton rb6 = new RadioButton("4 Players");
    rb6.setToggleGroup(group3);
    rb6.setUserData(4);
   
    rb5.setSelected(true);
    number = Integer.valueOf(group3.getSelectedToggle().getUserData().toString());
    
    group3.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
       @Override 
        public void changed(ObservableValue<? extends Toggle> ov,
          Toggle old_toggle, Toggle new_toggle) {
        if (group3.getSelectedToggle() != null) {
          System.out.println("in group3:    "+group3.getSelectedToggle().getUserData().toString());
          number = Integer.valueOf(group3.getSelectedToggle().getUserData().toString());
        }
      }
    });
    
    ////////////////////////////////////////////////////////////////////////////

    vbox = new VBox();
    
    Label label = new Label("Please select game settings");
    
    VBox vbox1 = new VBox();
    vbox1.getChildren().addAll(rb1, rb2);
    vbox1.setSpacing(10);
    
    VBox vbox2 = new VBox();
    vbox2.getChildren().addAll(rb3, rb4);
    vbox2.setSpacing(10);
    
    VBox vbox3 = new VBox();
    vbox3.getChildren().addAll(rb5, rb6);
    vbox3.setSpacing(10);
    
    StackPane nextPane = new StackPane();        
    Rectangle next = new Rectangle(70,35);
    next.setStrokeWidth(5);
    next.setFill(Color.GRAY.brighter());
    Text txt = new Text("Next");
    txt.setScaleX(1);
    txt.setScaleY(1);
    nextPane.getChildren().addAll(next, txt);
    
    nextPane.setOnMouseClicked(new EventHandler() {

        @Override
        public void handle(Event event) {
            
            ///////////////////////////////////
            ArrayList<String> x = new ArrayList<>(Arrays.asList("AI", "Human"));
            List<playerType> playerTypes = new ArrayList();
            List<ToggleGroup> groups = new ArrayList();
            vbox.getChildren().clear();
            
            System.out.println("number is: "+number);
            for (int i=0; i < number; i++){
                
                final ToggleGroup group = new ToggleGroup();
                VBox vbox1 = new VBox();

                    x.stream().forEach((et) -> {
                        RadioButton rb = new RadioButton(et);
                        rb.setToggleGroup(group);
                        rb.setUserData(et);
                        rb.setSelected(true);
                        vbox1.getChildren().add(rb);
                    });

                vbox1.setSpacing(10); 
                
                vbox.getChildren().add(vbox1);
                playerTypes.add(i, playerType.valueOf(group.getSelectedToggle().getUserData().toString()));
                
                final int I = i;
                group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
                @Override
                public void changed(ObservableValue<? extends Toggle> ov,
                    Toggle old_toggle, Toggle new_toggle) {
                        if (group.getSelectedToggle() != null) {
                          System.out.println("in playerTypes listener:  "+playerType.valueOf(group.getSelectedToggle().getUserData().toString()));
                          playerTypes.set(I, playerType.valueOf(group.getSelectedToggle().getUserData().toString()));
                        }
                    }
            });
                
                
                //////////////////////////////////////////////////////////////
                
                
//                VBox vbox1 = create(x).getFirst();
//                vbox.getChildren().add(vbox1);
//                groups.add(i, create(x).getSecond());
//                groups.add(group);
//                playerTypes.add(i, playerType.valueOf(groups.get(i).getSelectedToggle().getUserData().toString()));
//                
//                final int I = i;
//                groups.get(I).selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
//                @Override
//                public void changed(ObservableValue<? extends Toggle> ov,
//                    Toggle old_toggle, Toggle new_toggle) {
//                        if (groups.get(I).getSelectedToggle() != null) {
//                          System.out.println("in playerTypes listener:  "+playerType.valueOf(groups.get(I).getSelectedToggle().getUserData().toString()));
//                          playerTypes.add(I, playerType.valueOf(groups.get(I).getSelectedToggle().getUserData().toString()));
//                        }
//                    }
//            });
                
            }
            
            
            // create button
            StackPane startPane = new StackPane();        
            Rectangle start = new Rectangle(70,35);
            start.setStrokeWidth(5);
            start.setFill(Color.GRAY.brighter());
            Text txt = new Text("Start Game");
            txt.setScaleX(1);
            txt.setScaleY(1);
            startPane.getChildren().addAll(start, txt);
            startPane.setOnMouseClicked(new EventHandler() {

                
                @Override
                public void handle(Event event) {
                  
                System.out.println("in start game button:  "+playerTypes.size());    
                   
                final Node source = (Node) event.getSource();
                final Stage stage = (Stage) source.getScene().getWindow();
                stage.close();

                BoardUI m = new BoardUI(TS, CF, playerTypes);

                try {
                    m.start(new Stage());
                } catch (Exception ex) {
                    Logger.getLogger(MainMenu.class.getName()).log(Level.SEVERE, null, ex);
                }
                   
                }
            });
                    
                    
            vbox.getChildren().add(startPane);
            
        }
    });
    
    
    vbox.getChildren().addAll(label, vbox1, vbox2, vbox3, nextPane);
    vbox.setSpacing(50);
    vbox.setPadding(new Insets(20, 10, 10, 20));

    ((Group) scene.getRoot()).getChildren().add(vbox);
    stage.setScene(scene);
    stage.show();
    
    
  }
 
    public MyResult create(ArrayList<String> list){
        
        List<playerType> players = new ArrayList<>();
        final ToggleGroup group = new ToggleGroup();
        VBox vbox1 = new VBox();
        
            list.stream().forEach((et) -> {
                RadioButton rb = new RadioButton(et);
                rb.setToggleGroup(group);
                rb.setUserData(et);
                rb.setSelected(true);
                vbox1.getChildren().add(rb);
            });
            
        vbox1.setSpacing(10); 
//        group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
//                @Override
//                public void changed(ObservableValue<? extends Toggle> ov,
//                    Toggle old_toggle, Toggle new_toggle) {
//                        if (group.getSelectedToggle() != null) {
//                          System.out.println("in playerTypes listener:  "+playerType.valueOf(group.getSelectedToggle().getUserData().toString()));
//                          players.set(I, playerType.valueOf(group.getSelectedToggle().getUserData().toString()));
//                        }
//                    }
//            });
        
        
        return new MyResult(vbox1, group);
                }

    
}


final class MyResult {
    private final VBox vbox;
    private final ToggleGroup group;

    public MyResult(VBox first, ToggleGroup second) {
        this.vbox = first;
        this.group = second;
    }

    public VBox getFirst() {
        return vbox;
    }

    public ToggleGroup getSecond() {
        return group;
    }
}