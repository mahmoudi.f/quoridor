package com.jquoridor.GUI;

///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//


import com.jquoridors.core.Game;
import com.jquoridors.core.GameType.gameType1;
import com.jquoridors.core.GameType.gameType2;
import com.jquoridors.core.GameType.playerType;
import com.jquoridors.core.Tournament;
import com.jquoridors.core.Player;
import com.jquoridors.core.board.*;
import com.jquoridors.core.action.Action;
import com.jquoridors.core.action.move.PawnMove;
import com.jquoridors.core.action.move.FenceMove;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class BoardUI extends Application{

    public static CellUI[][] cells = new CellUI[9][9];
    public static WallUI[][] hWalls = new WallUI[8][9];
    public static WallUI[][] vWalls = new WallUI[8][9];
    public static Rectangle[][] intersections = new Rectangle[8][8];

    private static Group tileGroup = new Group();
    private static Group hWallGroup = new Group();
    private static Group vWallGroup = new Group();
    private static Group pawnGroup = new Group();
    private static Group intersectionsGroup = new Group();

    private HBox hbox = new HBox();
    private static Pane pane = new Pane();
    private static VBox gameInfo = new VBox();
    
    private static Game game;
    private static Tournament tournament;
    private static Integer numberOfPawns;
    List <Player> players = new ArrayList <>();
    private List<playerType> playerTypes;

    static Position[] PAWNS_DEFAULT_POS = { new Position('e', 9), new Position('e', 1), new Position('i', 5), new Position('a', 5)};
    static Color[] PAWNS_COLOR = {Color.RED, Color.YELLOW, Color.GREEN, Color.BLUE};
    
    private static List<BoardItem> pawns_list;
    private static List<BoardItem> walls_list;
    
    
    BoardUI(gameType1 ts, gameType2 cf, List<playerType> playerTypes) {
        
        this.playerTypes = playerTypes;
        BoardUI.numberOfPawns = playerTypes.size();
        
                
        tournament = new Tournament(ts, cf, playerTypes);
//        resetBoard(pane);
//        resetPawns(playerTypes.size());
    }
    
//     BoardUI(int numberOfPawns) {
//        BoardUI.numberOfPawns = numberOfPawns;
//
//        resetBoard(pane);
//        resetPawns(numberOfPawns);
//    }

     public static void resetGame() {
         resetBoard(pane);
         resetPawns(numberOfPawns);
         resetRightColumn();
     }
    
    public static final void resetBoard(Pane pane) {

        tileGroup.getChildren().clear();
        intersectionsGroup.getChildren().clear();
        hWallGroup.getChildren().clear();
        vWallGroup.getChildren().clear();
        pane.getChildren().clear();
        
        //cells
        for (Integer i = 0; i < 9; i++)
            for (Integer j = 0; j < 9; j++) {
                cells[i][j] = new CellUI(i, j);
                tileGroup.getChildren().add(cells[i][j]);
            }
        
        //intersections
        for (Integer i = 0; i < 8; i++)
            for (Integer j = 0; j < 8; j++) {
                intersections[i][j] = new Rectangle(15, 15);
                intersections[i][j].relocate(i*75+60, j*75+60);
                intersections[i][j].setFill(Color.LIGHTGRAY);
                intersectionsGroup.getChildren().add(intersections[i][j]);
            }
     
        //hWalls
        for (int j = 0; j < 9; j++){ 
            int J = j;
            for (int i = 0; i < 8; i++) {
                int I = i;
                hWalls[i][j] = new WallUI(j, i, FenceDirection.H);
                hWallGroup.getChildren().add(hWalls[i][j]);
                hWalls[i][j].setOnMouseEntered(new EventHandler() {
                    @Override
                    public void handle(Event event) {
                        if(tournament.getCurrentGame().getActionValidator().isValidBlockAction(new FenceMove(hWalls[I][J].getPosition(), FenceDirection.valueOf("H"))))
                        {
                            hWalls[I][J].setFill(Color.DARKGRAY.darker());
                            intersections[J][I].setFill(Color.DARKGRAY.darker());
                            hWalls[I][J+1].setFill(Color.DARKGRAY.darker());
                        }
                    }
                });
                hWalls[i][j].setOnMouseExited(new EventHandler() {
                    @Override
                    public void handle(Event event) {
                        if(tournament.getCurrentGame().getActionValidator().isValidBlockAction(new FenceMove(hWalls[I][J].getPosition(), FenceDirection.valueOf("H"))))
                        {
                        hWalls[I][J].setFill(Color.LIGHTGRAY);
                        intersections[J][I].setFill(Color.LIGHTGRAY);
                        hWalls[I][J+1].setFill(Color.LIGHTGRAY);
                        }
                    }
                });
//                
            }
        }
    
        //vWalls
        for (int j = 0; j < 9; j++) {
            int J = j;
            for (int i = 0; i < 8; i++) {
                int I = i;
                vWalls[i][j] = new WallUI(i, j, FenceDirection.V);
                vWallGroup.getChildren().add(vWalls[i][j]);
                vWalls[i][j].setOnMouseEntered(new EventHandler() {
                    @Override
                    public void handle(Event event) {
                        if(tournament.getCurrentGame().getActionValidator().isValidBlockAction(new FenceMove(vWalls[I][J].getPosition(), FenceDirection.valueOf("V"))))
                        {
                            vWalls[I][J].setFill(Color.DARKGRAY.darker());
                            intersections[I][J].setFill(Color.DARKGRAY.darker());
                            vWalls[I][J+1].setFill(Color.DARKGRAY.darker());
                        }
                    }
                });
                vWalls[i][j].setOnMouseExited(new EventHandler() {
                    @Override
                    public void handle(Event event) {
                        if(tournament.getCurrentGame().getActionValidator().isValidBlockAction(new FenceMove(vWalls[I][J].getPosition(), FenceDirection.valueOf("V"))))
                        {
                        vWalls[I][J].setFill(Color.LIGHTGRAY);
                        intersections[I][J].setFill(Color.LIGHTGRAY);
                        vWalls[I][J+1].setFill(Color.LIGHTGRAY);
                        }
                    }
                });
            }
        }
        
        pane.getChildren().addAll(tileGroup, hWallGroup, vWallGroup, pawnGroup, intersectionsGroup);

    }


    private static void resetRightColumn() {
        //set right column of the board, indicating number of each pawn's walls.
        for(Node pawn: pawnGroup.getChildren()) {
            PawnUI p = (PawnUI) pawn;
            Label l = new Label();
            l.setPadding(new Insets(20, 10, 10, 20));
            l.setTextFill(p.getColor());
            gameInfo.getChildren().add(l);
        }
    }

    public static final void resetPawns(Integer numberOfPawns){

        pawnGroup.getChildren().clear();
        
        // طبق ترتیبی که مهره ها در لاجیک برنامه شماره گذاری میشوند حتما باید اضافه شوند
        for (int i=0; i<numberOfPawns; i++){
            PawnUI pawn = new PawnUI(PAWNS_COLOR[i], PAWNS_DEFAULT_POS[i]);
            pawnGroup.getChildren().add(pawn);
        }
    }

    private static void showAdjacents(List<Position> validMoves, Position current) {

         for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    cells[i][j].setFill(Color.valueOf("FF9999"));
                    cells[i][j].setStroke(null);
                }
        }
         cells[current.getCol()-'a'][current.getRow()-1].setFill(Color.valueOf("FFCCCC"));
         validMoves.stream().forEach((pos) -> {
            cells[pos.getCol()-'a'][pos.getRow()-1].setFill(Color.valueOf("FFCCCC"));
        });
    }

    
    public static void showAlert(Pawn winner) { 
    Alert alert = new Alert(Alert.AlertType.INFORMATION);
    alert.setTitle("Congratulations");
    alert.setHeaderText("player "+winner.getColor().toString()+" won the tournament.");
    alert.showAndWait();
    //وقتی اوکیشو زد کلا همه چی بسته بشه
    }

    private static void updateBoard() {


        if (tournament.get_hasEnded() == true)
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    resetGame();
                    showAlert(tournament.getWinnerPawn());
                }
            });


            //وارد اینجا نمیشه هیچوقت.. چون وقتی یه بازی تموم میشه بلافاصله بازی بعد شروع میشه
        else if (tournament.getCurrentGame().get_hasEnded()) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    resetGame();
                }
            });
        }


        // بتونه لست اکشن رو بگیره و بر اساس اون یا جای اخرین مهره رو عوض کنه یا دیوار جدید بکشه!

        //put pawns in their new position      
        pawns_list = new ArrayList<>(tournament.getCurrentGame().getWorldModel().getBoard().getItems().get(BoardItemType.PAWN));
        for (int i = 0; i < numberOfPawns; i++) {
            Pawn pawn = (Pawn) pawns_list.get(i);
            Position pos = pawn.getPosition();
            PawnUI pawnUI = (PawnUI) pawnGroup.getChildren().get(i);        // شماره مهره های بورد باید با شماره مهره های یوآی منطبق باشد وگرنه ممکنه به هم بریزه
            pawnUI.setPosition(pos);
        }

        //show valid moves to player with a lighter color
        showAdjacents(tournament.getCurrentGame().getActionValidator().validMoves(tournament.getCurrentGame().getWorldModel().getCurrentPawn().getPosition()), tournament.getCurrentGame().getWorldModel().getCurrentPawn().getPosition());

        // show current player with stroked cell
        Position pos = tournament.getCurrentGame().getWorldModel().getCurrentPawn().getPosition();
        cells[pos.getCol() - 'a'][pos.getRow() - 1].setStrokeWidth(6);
        cells[pos.getCol() - 'a'][pos.getRow() - 1].setStroke(Color.valueOf("FF9999").darker());

        //reset walls
        for (int i = 0; i < 9; i++)
            for (int j = 0; j < 9; j++) {
                if (j==8 && i < 8)
                    vWalls[i][j].setFill(Color.LIGHTGRAY);
                else if (i==8 && j<8)
                    hWalls[j][i].setFill(Color.LIGHTGRAY);
                else if (i<8 && j<8){
                    hWalls[i][j].setFill(Color.LIGHTGRAY);
                    intersections[i][j].setFill(Color.LIGHTGRAY);
                    vWalls[i][j].setFill(Color.LIGHTGRAY);
                }
            }

        walls_list = new ArrayList<>(tournament.getCurrentGame().getWorldModel().getBoard().getItems().get(BoardItemType.WALL));
        walls_list.stream().map((wallItem) -> (Wall) wallItem).forEach((wall) -> {
            Position wallPos = wall.getPosition();
            if (wall.getOrientation() == FenceDirection.H) {
                hWalls[wallPos.getRow()-1][wallPos.getCol()-'a'].setFill(Color.DARKGRAY.darker());
                intersections[wallPos.getCol() - 'a'][wallPos.getRow() - 1].setFill(Color.DARKGRAY.darker());
                hWalls[wallPos.getRow()-1][wallPos.getCol()-'a'+1].setFill(Color.DARKGRAY.darker());
                
            }
            else if (wall.getOrientation() == FenceDirection.V) {
                vWalls[wallPos.getCol() - 'a'][wallPos.getRow() - 1].setFill(Color.DARKGRAY.darker());
                intersections[wallPos.getCol() - 'a'][wallPos.getRow() - 1].setFill(Color.DARKGRAY.darker());
                vWalls[wallPos.getCol()-'a'][wallPos.getRow()].setFill(Color.DARKGRAY.darker());
            }

        });

        //update number of walls
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                for(int i=0; i<numberOfPawns; i++) {
                    Label l = (Label) gameInfo.getChildren().get(i);
                    l.setText("player "+(i+1)+" has " + tournament.getCurrentGame().getWorldModel()
                            .getRemainingWalls(i + 1) + " walls remaining.");
                }
            }
        });


    }


    public static Action getInputs() {

        updateBoard();
        AtomicReference<Position> pos = new AtomicReference<>(null);
        AtomicReference<Position> wallPos_h = new AtomicReference<>(null);
        AtomicReference<Position> wallPos_v = new AtomicReference<>(null);

        while (true) {
            //cells click
            for (int i = 0; i < 9; i++) {
                int I = i;
                for (int j = 0; j < 9; j++) {
                    int J = j;
                    cells[I][J].setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            pos.set(cells[I][J].getPosition());
                        }
                    });
                    if (pos.get() != null) {
                        return new PawnMove(pos.get() , tournament.getCurrentGame().getWorldModel().getCurrentPawn().getPosition());
                    }
                }
            }

            // hwalls click
            for (int j = 0; j < 9; j++) {
                int J = j;
                for (int i = 0; i < 8; i++) {
                    int I=i;
                    hWalls[I][J].setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent e) {
                              wallPos_h.set(hWalls[I][J].getPosition());
                        }
                    });

                    if (wallPos_h.get() != null) {
                        return new FenceMove(wallPos_h.get(), FenceDirection.valueOf("H"));
                    }

                }
            }

//            //vWalls click
            for (int j = 0; j < 9; j++) {
                int J = j;
                for (int i = 0; i < 8; i++) {
                    int I=i;
                    vWalls[I][J].setOnMouseClicked(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent e) {
                              wallPos_v.set(vWalls[I][J].getPosition());
                        }
                    });

                    if (wallPos_v.get() != null) {
                        return new FenceMove(wallPos_v.get(), FenceDirection.valueOf("V"));
                    }
                }
            }
        }
    }



    @Override
    public void start(Stage primaryStage) throws Exception {
        

        resetBoard(pane);
        resetPawns(playerTypes.size());
        resetRightColumn();

        hbox.getChildren().addAll(pane, gameInfo);

        Scene scene = new Scene(hbox, 860, 660);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Quoridor");
        primaryStage.show();

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                tournament.run();
            }
        };
        timer.scheduleAtFixedRate(task, 5, 700);
    }


    public static void main(String[] args) {
        launch(args);
    }


  
    
}





