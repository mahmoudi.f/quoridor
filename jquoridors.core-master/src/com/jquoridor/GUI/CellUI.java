package com.jquoridor.GUI;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fm
 */

import com.jquoridors.core.board.Position;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;


public class CellUI extends Rectangle {

    public static final int CELL_SIZE = 60;
    private Position position;
    private int x, y;
    

    public CellUI(int x, int y) {
        
        this.x = x;
        this.y = y;
        
        this.position = new Position((Character)((char)(x + 'a')), y+1);
        
        setWidth(CELL_SIZE);
        setHeight(CELL_SIZE);
        
        relocate(x*75, y*75);
        setFill(Color.valueOf("FF9999"));
    } 
   
   public Position getPosition(){
       return this.position;
   }

}
