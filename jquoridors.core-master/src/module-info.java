module jquoridors.core {

    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.base;
    requires java.desktop;
    requires org.jgrapht.core;
    requires org.jgrapht.io;
    requires java.logging;
    requires java.base;

    opens com.jquoridor.GUI;
}
